/*
 * Author: lukequaint
 */

#ifndef SDL_TOOLS_H
#define SDL_TOOLS_H

#include "SDL/SDL.h"
#include "SDL/SDL_gfxPrimitives.h"
#include <cmath>
#include <cstdio>
#include <iostream>
#include <set>
#include <string>
#include <sstream>
#include <vector>

const int WIDTH = 1100;
const int HEIGHT = 800;
const int BPP = 32;

float deg2rad(float deg);
std::string int2str(int number);
int init_SDL();

/*
 * Timer class from Lazy Foo' Productions
 * lazyfoo.net
 *
 * */

//The timer
class Timer {
        private:
                //The clock time when the timer started
                long long startTicks;

                //The ticks stored when the timer was paused
                long long pausedTicks;

                //The timer status
                bool paused;
                bool started;

        public:
                //Initializes variables
                Timer();

                //The various clock actions
                void start();
                void stop();
                void pause();
                void unpause();

                //Gets the timer's time
                int get_ticks();

                //Checks the status of the timer
                bool is_started();
                bool is_paused();
};


#endif
