/*
 * Author: lukequaint
 */

#include "sdl_tools.h"
#include <Box2D/Box2D.h>
#include <vector>
#include <fstream>
#include <set>
using namespace std;

// Frames per second setting
const int FPS = 60;

// Meters -> pixels coefficient
const int MP_COEF = 10.0f;

// Size of ball
const float BALL_RADIUS = 0.7f;

// The surfaces
extern SDL_Surface *screen;

// Time step - frequency
float32 timeStep = 1.0f / 60.0f;

// Cycle to count velocity and positions
int32 velocityIterations = 6;
int32 positionIterations = 2;

// Define the gravity vector
b2Vec2 gravity(0.0f, 9.8f);
b2Vec2 body_position;

// Construct a world object, which will hold and simulate the rigid
// bodies.
b2World world(gravity);

// Ground size
float ground_length0 = 90.0f;
float ground_length1 = 30.0f;

// Loop size
float radius = 20.0f;

// Friction of ground and ball
float friction = 0.0f;

float loop_angle = deg2rad(45.0);
float ground_angle = deg2rad(45.0);
pair<float, float> ground_position0(1.0f, 2.0f);
pair<float, float> ramp_end(ground_position0.first + ground_length0 * cos(ground_angle),
        ground_position0.second + ground_length0 * sin(ground_angle));
pair<int, int> ground_position0_draw(int(ground_position0.first * MP_COEF),
        int(ground_position0.second * MP_COEF));

pair<float, float> loop_centre;
pair<float, float> ground_position1;
pair<int, int> ground_position1_draw;


pair<int, int> ramp_end_draw(int(ramp_end.first * 10),
        int(ramp_end.second * 10));


// Ground vertices
vector<pair<int, int> > vertices_draw_left;
vector<pair<int, int> > vertices_draw_right;
vector<pair<pair<float, float>, float> > vertices_left;
vector<pair<pair<float, float>, float> > vertices_right;

// Loop segments (to delete)
vector<b2Body*> ground_segments;

void input()
{
    ifstream is("data.txt");
    float ls = 0.0, fr = 0.0; // loop size and friction
    is >> ls;
    is >> fr;
    ls = max(ls, 5.0f);
    ls = min(ls, 30.0f);
    fr = max(fr, 0.0f);
    fr = min(fr, 1.0f);
    radius = ls;
    friction = fr;
}

// Because of late radius setting
void initialize()
{
    loop_centre.first = ramp_end.first + radius / sqrt(2);
    loop_centre.second = ramp_end.second - radius / sqrt(2);
    ground_position1.first = loop_centre.first;
    ground_position1.second = loop_centre.second + radius;
    ground_position1_draw.first = int(ground_position1.first * MP_COEF);
    ground_position1_draw.second = int(ground_position1.second * MP_COEF);
}

void generate_vertices() {
    float x0 = 0.0, y0 = 0.0;
    float x1 = 0.0, y1 = 0.0;
    float angle = loop_angle;
    float angle_step = -0.1f / radius;
    float full_angle = 0.0f;
    float limit = -M_PI;
    float limit2 = -M_PI * 2.17f;

    x0 = ramp_end.first; 
    y0 = ramp_end.second;

    // Right arc
    while (full_angle > limit) {
        x1 = x0 + 0.1 * cos(angle);
        y1 = y0 + 0.1 * sin(angle);
        angle += angle_step;
        full_angle += angle_step;

        // Add vertex to draw (without duplicates)
        if ((int(MP_COEF * x1) != int(MP_COEF * x0)) 
                || (int(MP_COEF * y1) != int(MP_COEF * y0))) {
            vertices_draw_right.push_back(make_pair(int(MP_COEF * x1), int(MP_COEF * y1)));
        }

        // Add segment to circle approximation
        vertices_right.push_back(make_pair(make_pair(x1, y1), angle));
        x0 = x1;
        y0 = y1;
    }

    // Left arc
    while (full_angle > limit2) {
        x1 = x0 + 0.1 * cos(angle);
        y1 = y0 + 0.1 * sin(angle);
        angle += angle_step;
        full_angle += angle_step;

        // Add vertex to draw (without duplicates)
        if ((int(MP_COEF * x1) != int(MP_COEF * x0)) 
                || (int(MP_COEF * y1) != int(MP_COEF * y0))) {
            vertices_draw_left.push_back(make_pair(int(x1 * MP_COEF), int(y1 * MP_COEF)));
        }

        // Add segment to circle approximation
        vertices_left.push_back(make_pair(make_pair(x1, y1), angle));
        x0 = x1;
        y0 = y1;
    }
}

void create_ground() {
    // Define the ground body
    b2BodyDef groundBodyDef0, groundBodyDef1;
    groundBodyDef0.type = b2_staticBody;
    groundBodyDef1.type = b2_staticBody;
    groundBodyDef0.position.Set(ground_position0.first, ground_position0.second);
    groundBodyDef1.position.Set(ground_position1.first, ground_position1.second);
    groundBodyDef0.angle = ground_angle;

    // Call the body factory which allocates memory for the ground body
    // from a pool and creates the ground box shape (also from a pool).
    // The body is also added to the world.
    b2Body* groundBody0 = world.CreateBody(&groundBodyDef0);
    b2Body* groundBody1 = world.CreateBody(&groundBodyDef1);

    // Define the ground box shape.
    b2PolygonShape groundBox0;
    b2PolygonShape groundBox1;
    groundBox0.SetAsBox(ground_length0, 0.1f);
    groundBox1.SetAsBox(ground_length1, 0.1f);

    // Friction part
    b2FixtureDef bodyfixtureDef0;
    b2FixtureDef bodyfixtureDef1;
    bodyfixtureDef0.shape = &groundBox0;
    bodyfixtureDef1.shape = &groundBox1;
    bodyfixtureDef0.friction = friction;
    bodyfixtureDef0.restitution = 0.0; // Disable bouncing
    bodyfixtureDef1.friction = friction;

    // Add the ground fixture to the ground body.
    groundBody0->CreateFixture(&bodyfixtureDef0);
    groundBody1->CreateFixture(&bodyfixtureDef1);
}

void create_loop(vector<pair<pair<float, float>, float> > &vertices, bool add_segment) {
    // Pointers to circle parts
    b2BodyDef BodyDef;
    b2Body* Body;
    b2PolygonShape *Shape;
    b2FixtureDef segfixtureDef;

    for (int i = 0; i < int(vertices.size()); ++i) {
        float x = vertices[i].first.first;
        float y = vertices[i].first.second;
        float angle = vertices[i].second;

        Shape = new b2PolygonShape;

        // Inclination
        BodyDef.position.Set(x, y);
        BodyDef.angle = angle;

        // Same as above
        Body = world.CreateBody(&BodyDef);

        if (add_segment) {
            ground_segments.push_back(Body); 
        }
        segfixtureDef.shape = Shape;
        segfixtureDef.friction = friction;
        segfixtureDef.restitution = 0.0; // Disable bouncing
        Shape->SetAsBox(0.1f, 0.0f);
        Body->CreateFixture(&segfixtureDef);
        delete Shape;
    }

}

void delete_ground() {
    int size = ground_segments.size();
    for (int i = 0; i < size; ++i) {
        world.DestroyBody(ground_segments[i]);
    }
}

b2Body* create_ball() {
    b2BodyDef bodyDef;
    bodyDef.type = b2_dynamicBody; // Moving in response to forces
    bodyDef.position.Set(1.0f, 1.0f);
    bodyDef.fixedRotation = true; // Prevents from rotating - for friction
    b2Body* body = world.CreateBody(&bodyDef);

    b2CircleShape circle;
    circle.m_radius = BALL_RADIUS;

    // Bind body to shape and add material properties
    b2FixtureDef fixtureDef;
    fixtureDef.shape = &circle;
    fixtureDef.density = 1.0f;
    fixtureDef.friction = friction;
    fixtureDef.restitution = 0.0;

    // Add fixture to body
    body->CreateFixture(&fixtureDef);
    return body;
}

void draw_loop(SDL_Surface *screen) {
    // Clearing and drawing
    SDL_FillRect(screen, &screen->clip_rect, SDL_MapRGB(screen->format, 
                0x00, 0x00, 0x00));

    lineRGBA(screen, ground_position0_draw.first, ground_position0_draw.second, 
            ramp_end_draw.first, ramp_end_draw.second, 
            0xFF, 0x00, 0x00, 0xFF);
    lineRGBA(screen, ground_position1_draw.first, ground_position1_draw.second, 
            ground_position1_draw.first + ground_length1 * MP_COEF, ground_position1_draw.second, 
            0xFF, 0x00, 0x00, 0xFF);

    filledEllipseRGBA(screen, MP_COEF * (body_position.x), MP_COEF * (body_position.y),
            MP_COEF * (BALL_RADIUS), MP_COEF * (BALL_RADIUS),
            0xF0, 0x28, 0x00, 0xFF);

    int vertices_number_right = vertices_draw_right.size();
    int vertices_number_left  = vertices_draw_left.size();

    for (int i = 0; i < vertices_number_right; ++i) {
        pixelRGBA(screen, 
                vertices_draw_right[i].first, 
                vertices_draw_right[i].second, 
                0xFF, 0x00, 0x00, 0xFF);
    }

    for (int i = 0; i < vertices_number_left; ++i) {
        pixelRGBA(screen, 
                vertices_draw_left[i].first, 
                vertices_draw_left[i].second, 
                0xFF, 0x00, 0x00, 0xFF);
    }
}

int main(int argc, char* argv[]) 
{
    input();
    initialize();

    init_SDL();

    // Running real-time loop
    bool running = true;

    B2_NOT_USED(argc);
    B2_NOT_USED(argv);

    // Set calculating frequency
    world.Step(timeStep, velocityIterations, positionIterations);

    generate_vertices();

    create_ground();
    create_loop(vertices_right, true);
    b2Body* body = create_ball();

    // Number of frames
    int frames = 0;
    Timer fps;
    Timer update;
    update.start();

    body_position = body->GetPosition();

    bool gate0 = false;
    bool gate1 = false;

    while (running) {
        // Box2D part
        fps.start();
        world.Step(timeStep, velocityIterations, positionIterations);

        // Events and drawing - SDL part
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_KEYDOWN) {
                if (event.key.keysym.sym == SDLK_ESCAPE) {
                    running = false;
                }
            }
        }
        body_position = body->GetPosition();

        draw_loop(screen);

        // Frames per second 
        frames++;
        if (fps.get_ticks() < (1000 / FPS)) {
            SDL_Delay((1000 / FPS) - fps.get_ticks());
        }

        if (!gate0 && body_position.x > ramp_end.first + 3) {
            gate0 = true;
            create_loop(vertices_left, false);
        }
        if (!gate1 && gate0 && body_position.x < ramp_end.second) {
            delete_ground();
            gate1 = true;
        }
        //int fps = frames / (update.get_ticks() / 1000.f);
        //fps_text = TTF_RenderText_Solid(font, (int2str(fps)).c_str(), textColor);
        //apply_surface(480, 0, fps_text, screen);

        if (SDL_Flip(screen) == -1) {
            return 1;
        }
    }
    SDL_Quit();
}
