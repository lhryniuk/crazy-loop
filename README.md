# crazy_loop

This is a one of my very first projects. I've found it on my disk some time ago. It's a small simulation of a ball on a loop using Box2D and SDL. **It should stay as it is**, for sentimental reasons :smiley:.

## Building

As above, it depends on a few parts of SDL library and Box2D. After installing them, execute `make` and run `./sim`.

## Settings in `data.txt`:
* first number - loop size: values between 5 and 30 work well
* second line - friction: 0.0-1.0 (above 1.0 is also fine, but nothing change)
