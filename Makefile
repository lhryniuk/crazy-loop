FLAGS =  -lBox2D -lSDL -lSDL_gfx -g
SRC = $(wildcard *.cpp)
OBJ = $(SRC:.cpp=.o)
NAME=sim

all: \
	sim \
	clean

sim: $(OBJ)
	g++ -o $(NAME) $(OBJ) $(FLAGS)

%.o: %.cpp
	g++ -o $@ -c $< -W -Wall

clean:
	rm *.o

.PHONY: clean
